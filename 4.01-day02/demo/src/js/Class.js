import React, { Component } from 'react';
import "./Class.css"
//点击变色
export default class Class extends Component {
    constructor(props) {
        super(props)

        this.state = {
            list: [{ name: "小明" }, { name: "小鹏" }, { name: "小华" }, { name: "小飞" },],
            index:-1
        }
    }
    show = (index) => {
        this.setState({
            index,
        })
    }

    render() {
        return (
            <div>
                {
                    this.state.list.map((item, index) => {
                        return <li key={index} onClick={() => { this.show(index) }} className={this.state.index==index?"active":""}>{item.name}</li>
                    })
                }
            </div>
        )
    }
}
