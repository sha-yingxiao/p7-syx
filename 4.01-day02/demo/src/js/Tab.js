import React, { Component } from 'react'
import "./Tab.css"
//Tab切换
export default class tab extends Component {
    constructor(props) {
        super(props)

        this.state = {
            list: [1, 2, 3],
            index: 0,
            cat: [1, 2, 3]
        }
    }
    change(index) {
        this.setState((state, props) => {
            return {
                index
            }
        })
    }
    render() {
        return (
            <div>
                <div className="box">
                    <ul className="top">
                        {this.state.list.map((item, index) => {
                            return <li key={index} className={this.state.index == index ? "active" : " "} onClick={
                                () => {
                                    this.change(index)
                                }
                            }>{item}</li>
                        })}
                    </ul>
                    <ul className="foot">
                        {this.state.cat.map((item,index) => {
                            return <li key={index} className={this.state.index == index ? "active" : " "}>{item}</li>
                        })}
                    </ul>
                </div>
            </div>
        )
    }
}
