import React, { Component } from 'react'

export default class List extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    render() {
        return (
            <div>
                  <h2>已完成任务</h2>
                <ul>
                    {this.props.list.map((item, index) => {
                        return <li key={index} className={item.status == 0 ? "hidde" : ""}>
                            <input type="checkbox" onChange={() => { this.props.change(index) }} 
                            checked={item.status}/>
                            {item.title}
                        </li>
                    })}
                </ul>
                <h2>未完成任务</h2>
                <ul>
                    {this.props.list.map((item, index) => {
                        return <li key={index} className={item.status == 1 ? "hidde" : ""}>
                            <input type="checkbox" onChange={() => { this.props.change(index) }} checked={item.status}/>
                            {item.title}
                        </li>
                    })}
                </ul>
            </div>
        )
    }
}
