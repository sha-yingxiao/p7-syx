import React, { Component } from 'react'
import "./Todo.css"
import List from "./List"
import Add from "./Add"

export default class Todo extends Component {
    constructor(props) {
        super(props)

        this.state = {
            list: [{
                id: 1,
                title: "吃饭",
                status: 0,
            }, {
                id: 2,
                title: "睡觉",
                status: 1,
            }, {
                id: 3,
                title: "打豆豆",
                status: 0,
            }, {
                id: 4,
                title: "跑步",
                status: 1,
            }],
            title: ""
        }
    }
    // getTitle = (e) => {
    //     this.setState({
    //         title: e.target.value,
    //     })
    // }
    add = (title) => {
        this.setState({
            list: [
                ...this.state.list,
                {
                    id: this.state.list.length + 1,
                    title:title,
                    status: 0,
                }],
            // title: ""
        })
    }
    change=(index)=>{
        var res=this.state.list;
        res[index].status=!res[index].status;
        this.setState({
          list:res,
        })
    }
    render() {
        return (
            <div>
                <h1>{this.props.msg}</h1>
              <Add add={this.add}/>
                <hr />
              <List list={this.state.list} change={this.change}/>
            </div>
        )
    }
}
