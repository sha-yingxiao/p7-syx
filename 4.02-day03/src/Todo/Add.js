import React, { Component } from 'react'

export default class Add extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            title: ""
        }
    }
    getTitle = (e) => {
        this.setState({
            title: e.target.value,
        })
    }

    render() {
        return (
            <div>
                <input type="text" value={this.state.title} onChange={this.getTitle} />
                <button onClick={()=>{this.props.add(this.state.title)}}>add</button>
            </div>
        )
    }
}
