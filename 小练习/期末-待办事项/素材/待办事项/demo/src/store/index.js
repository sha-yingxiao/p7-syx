import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import vuexPersist from "vuex-persist";


export default new Vuex.Store({
  state: {
    list:[],
    color:''
  },
  mutations: {
    add(state,i){
      state.list.push({
        title:i,
        num:0,
        time:new Date().toLocaleString()
      })
    },
    red(state,red){
      state.color=red
    },
    pink(state,pink){
      state.color=pink
    },
    yellowgreen(state,yellowgreen){
      state.color=yellowgreen
    }
    
  },
  actions: {
  },
  modules: {
  },
  plugins: [
    new vuexPersist({
      storage: window.localStorage,
    }).plugin,
  ],
})
