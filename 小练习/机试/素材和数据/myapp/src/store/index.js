import Vue from 'vue'
import Vuex from 'vuex'
import name from 'vuex-persist'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tab:[],
    // 切换列表
    shopping:[],
    // 我的购物车
  },
  mutations: {
    addTab(state,data){
      state.tab = data;
    },
    add(state,data){
      let flag = false;
      state.shopping.forEach(itme=>{
        if(itme.name == data.itme.name) {
          flag = true;
        }
      })
      if(!flag) {
        state.tab[data.index].level = 1;
        state.shopping.push(data.itme);
      }
    },
    del(state,data){
      state.shopping.splice(data.index,1);
      state.tab.forEach((itme,index)=>{
        if(itme.name == data.itme.name) {
          state.tab[index].level = -1;
        }
      })
    }
  },
  actions: {},
  modules: {},
  plugins: [
    new name({
      storage: window.localStorage,
    }).plugin,
  ],
})