import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import ('../views/Home.vue'),
    redirect:'/home'
  },{
    path:'/index',
    component: () => import ('../views/Index.vue'),
    children:[{
      path:'/home',
      component: () => import ('../views/Home.vue')
    }]
  },{
    path:'/comment',
    component: () => import ('../views/Comment.vue'),
    redirect:'/s',
    children:[{
      path:'/s',
      component: () => import ('../views/S.vue')
    },{
      path:'/f',
      component: () => import ('../views/F.vue')
    }]
  }
]

const router = new VueRouter({
  routes
})

export default router
