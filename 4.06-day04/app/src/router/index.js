import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect:'/home'
  },{
    path:'/index',
    component: () => import ('../views/index/Index.vue'),
    children:[{
      path:'/home',
      component: () => import ('../views/index/Home.vue')
    },{
      path:'/list',
      component:() => import ('../views/index/List.vue')
    },{
      path:'/shopping',
      component: () => import ('../views/index/Shopping.vue')
    },{
      path:'/my',
      component: () => import ('../views/index/My.vue')
    }]
  },{
    path:'/kanList',
    component: () => import ('../views/KanList.vue')
  },{
    path:'/news',
    component: () => import ('../views/News.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
