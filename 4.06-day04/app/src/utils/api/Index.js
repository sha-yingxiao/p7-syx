import http from "./request.js";
// 专门放请求主页数据的接口

async function getbanner() {
    let res = await http("/small4/banner/list");
    return res.data;
}
// 轮播请求

async function getKanjia () {
    let res = await http("/small4/shop/goods/kanjia/list")
    return res.data;
}
// 砍价
async function news (){
    let res = await http('/small4/cms/news/list');
    return res.data;
}
// 专栏
async function getgoods () {
    let res = await http('/small4/shop/goods/list');
    return res.data;
}
// 人气推荐
export { getbanner,getKanjia,news,getgoods }